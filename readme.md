NPM package of grunt-wp-travel-addon-generator.

## commands

### install
`wp-travel-addon install ${addon_name}`
It will install the WP Travel addon. It clones the main addon repo, switches branch to dev and generates files(withour cloning grunt-wp-travel-addon-generator). 

### generate
`wp-travel-addon generate`
You must be inside the plugin dir and running the command will generate the files. It is similar to `grunt writefile`.