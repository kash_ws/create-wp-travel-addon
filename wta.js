#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const cp = require('child_process')
const p = require('process')
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const hbs = require('handlebars')

hbs.registerHelper({
    upperCase: function (value) {
        return value.toUpperCase();
    },
    lowerCase: function (value) {
        return value.toLowerCase();
    },
    removePrefix: function (value) {
        return value.replace('wp-travel-', '');
    },
    eq: function (v1, v2) {
        return v1 === v2;
    },
    ne: function (v1, v2) {
        return v1 !== v2;
    },
    lt: function (v1, v2) {
        return v1 < v2;
    },
    gt: function (v1, v2) {
        return v1 > v2;
    },
    lte: function (v1, v2) {
        return v1 <= v2;
    },
    gte: function (v1, v2) {
        return v1 >= v2;
    },
    and: function () {
        return Array.prototype.slice.call(arguments).every(Boolean);
    },
    or: function () {
        return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
    }
});

const addonRoot = p.cwd()

const actions = {}
actions.install = () => {
    rl.question("Enter addon name:", async name => {
        await cp.exec('rm -r wp-travel-utilities', { cwd: addonRoot })
        // name = 'wp-travel-utilities'
        const pluginDir = path.join(addonRoot, `${name}`)
        cp.exec(`git clone git@gitlab.com:ws-plugins/${name}.git "${pluginDir}"`, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: 'cloned successfully.'`);
                console.log('Changing branch to dev.')
                cp.exec(`git checkout dev`, { cwd: pluginDir }, (err, stdout, stderr) => {
                    console.log('Successfully Checkedout to dev.')
                    actions.generate(pluginDir)
                })
                return;
            }
            console.log(`stdout: ${stdout}`);
        })
    })
}

actions.create = (name) => {

}

actions.generate = (pluginDir) => {
    pluginDir
    let packageJson = fs.readFileSync(path.join(pluginDir, 'package.json'), 'utf8')
    packageJson = packageJson && JSON.parse(packageJson)

    let pluginInfo = packageJson.plugin_property;
    pluginInfo.slug = packageJson.name;
    pluginInfo.version = packageJson.version;
    pluginInfo.description = packageJson.description;
    pluginInfo.textDomain = ('undefined' === typeof pluginInfo.textDomain) ? packageJson.name : pluginInfo.textDomain;

    const fileList = [{
        src: 'templates/main-file.hbs',
        dest: `${pluginInfo.slug}.php`,
    }, {
        src: 'templates/inc/class-modules.hbs',
        dest: `inc/class-modules.php`,
    }, {
        src: 'templates/gitignore.hbs',
        dest: `.gitignore`,
    }, {
        src: 'templates/gitmodules.hbs',
        dest: `.gitmodules`,
    }, {
        src: 'templates/editorconfig.hbs',
        dest: `.editorconfig`,
    }, {
        src: 'templates/Gruntfile.hbs',
        dest: `Gruntfile.js`,
    }, {
        src: 'templates/loco.hbs',
        dest: `loco.xml`,
    }, {
        src: path.join(pluginDir, 'README.hbs'),
        dest: `readme.txt`,
    }, {
        src: 'templates/readmemd.hbs',
        dest: `README.md`,
    }, {
        src: 'templates/bash/pre_release.hbs',
        dest: `bash/pre_release.sh`,
    }];

    console.log(pluginInfo)
    console.log(fileList)
    fileList.forEach(file => {
        if (fs.existsSync(path.resolve(__dirname, file.src))) {
            const readPath = path.resolve(__dirname, file.src)
            const writePath = path.resolve(pluginDir, file.dest)
            const templateContent = fs.readFileSync(readPath, 'utf8')
            const template = hbs.compile(templateContent)
            const newTemplate = template({ ...pluginInfo })
            if (!fs.existsSync(path.dirname(writePath))) {
                fs.mkdirSync(path.dirname(writePath))
            }
            fs.writeFileSync(writePath, newTemplate)
        }
    })
}

if (p.argv.length > 2) {
    const actionName = p.argv[2]
    if (typeof actions[actionName] === 'function') {
        if(actionName === 'generate') {
            actions.generate(p.cwd())
        }else {
            actions[actionName]()
        }
    } else {
        console.log('Invalid Parameter')
        process.exit(0);
    }
} else {
    console.log('No arguments passed')
    process.exit(0);
}

